//
//  ViewController.swift
//  iOSOldDriver
//
//  Created by HamGuy on 9/29/16.
//  Copyright © 2016 HamGuy. All rights reserved.
//

import UIKit
import HGToolKit

enum SkiilItem: Int{
    case uikit  //[UICollectionViewLayot]
    case controls_views
    case storyBoard
    case coredata_Sqlite
    case framework
    case animations
    case autolayout
    case testcase
    case gesture
    case motions
    case extensions
    case runtime  //[Message Forwarding, AssoiatedObject, MethodSwizzing]
    case runloop
    case mulltithread //[GCD, NSoperationQueue]
    case memory //[MRC, RetainCycle]
    case textHanding //[Core Text, Textkit]
    case imageHanding //[Core Image, GPUImage]
    case coreGraphic //[]
    case blockClosure //[Block, Closure]
    case hybird // [WebjavascriptBridage, JavaScriptCore, WKWebview]
    case reactNative
    case reactive // [Reactive Cocoa, Reactive Swif]
    case designPattern
    case algorim
    case security
    case utils
}


class MainViewController: UIViewController {
    
    fileprivate var itemsList: [[String:Any]] = []
    fileprivate var expandedSections: [Int] = []
    fileprivate var selectedIndex = -1
    
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.        
        
        title = "iOS Skills"
        
        let plistFile = PlistFile(fileName: "FuntionList")
        if let list = plistFile.arrayValue as? [[String:Any]]{
            itemsList = list
        }
    }
}

extension MainViewController: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return itemsList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if expandedSections.contains(section){
            let dict = itemsList[section]
            let key = dict.keys.first!
            if let names = dict[key] as? [String]{
                return names.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "functionCell", for: indexPath)
        let dict = itemsList[indexPath.section]
        let key = dict.keys.first!
        let names = dict[key] as! [String]
        cell.textLabel?.text = names[indexPath.row]
        cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
        return cell
    }
    
    
}

extension MainViewController: UITableViewDelegate{

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.width, height: 36))
        headerView.backgroundColor = UIColor(decRed: 249, decGreen: 249, decBlue: 249)
        let label = UILabel(frame: headerView.bounds.insetBy(dx: 15, dy: 0))
        label.backgroundColor = UIColor.clear
        label.font = UIFont.boldSystemFont(ofSize: 17)
        let dict = itemsList[section]
        label.text = dict.keys.first
        
        let actionButton = UIButton(frame: CGRect(x: headerView.width - 63, y: 7, width: 48, height: 21))
        actionButton.setImage(UIImage(named:"DownArrow"), for: .normal)
        actionButton.tag = section
        actionButton.addTarget(self, action: #selector(showorHideItems(sender:)), for: .touchUpInside)
        
        let key = dict.keys.first!
        let names = dict[key] as! [String]
        actionButton.isHidden = names.count == 0
        
        actionButton.transform  = CGAffineTransform(rotationAngle: expandedSections.contains(section) ? CGFloat.pi : 0)
        
        headerView.addSubview(actionButton)
        headerView.addSubview(label)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 36
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        gotoController(section: indexPath.section, item: indexPath.item)
    }
    
    @IBAction func showorHideItems(sender:UIButton){
        let currentIndex = sender.tag
        if expandedSections.contains(currentIndex){
            if currentIndex != selectedIndex{
                expandedSections.removeObject(selectedIndex)
            }
            expandedSections.removeObject(currentIndex)
        }else{
            if expandedSections.contains(selectedIndex){
                expandedSections.removeObject(selectedIndex)
            }
            expandedSections.append(currentIndex)
        }
        selectedIndex = currentIndex
        tableView.reloadData()
        if expandedSections.count>0{
            tableView.scrollToRow(at: IndexPath(row: 0, section: expandedSections.first!), at: .top, animated: true)
        }
    }
    
    func gotoController(section:Int, item:Int){
//        guard let storyboard = storyboard else { return }
        var controller: UIViewController? = nil
        let skill = SkiilItem(rawValue: section)!
        switch skill {
        case .uikit:
            if item == 0{
                if item == 0{
                    controller = CollectionViewLayoutsController()
                }
            }
            break
        case .utils:
            if item == 0{
                controller = UIMotionEffectTestController()
            }
        default:
            break
        }
        if let theController = controller{
            navigationController?.pushViewController(theController, animated: true)
        }
    }
}

