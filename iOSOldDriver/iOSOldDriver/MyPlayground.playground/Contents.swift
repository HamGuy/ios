//: Playground - noun: a place where people can play

import UIKit
import HGToolKit

var str = "Hello, playground"

let example = ["hello.swift", "word.c", "functinal.swift"]


let maps = example.map { "fileName:" + $0}
print(maps)

let filiers = example.filter { $0.hasSuffix("c") }
print(filiers)

let reduces = example.reduce("example") { result, x in
    let a = "File:" + x
    return  result + "\n" + a
}
print(reduces)

func tests(x:Any)->Any{
    return x
}


func test<T>(t:T)->T{
    return t
}


print(test(t: tests(x: 2)))


extension String {
    init(htmlEncodedString: String) {
        self.init()
        guard let encodedData = htmlEncodedString.data(using: .utf8) else {
            self = htmlEncodedString
            return
        }
        
        let attributedOptions: [String : Any] = [
            NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
            NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue
        ]
        
        do {
            let attributedString = try NSAttributedString(data: encodedData, options: attributedOptions, documentAttributes: nil)
            self = attributedString.string
        } catch {
            print("Error: \(error)")
            self = htmlEncodedString
        }
    }
}

let testStr = String(htmlEncodedString: "红红火火")

let fileName = "xxxx.plist"
let name = fileName.hasSuffix(".plist") ? fileName.subStringToIndex(endIndex: fileName.length-6) : fileName

let plistFile = PlistFile(fileName: "FuntionList")
let arrau = plistFile.arrayValue

