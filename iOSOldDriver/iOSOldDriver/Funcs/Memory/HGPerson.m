//
//  HGPerson.m
//  iOSOldDriver
//
//  Created by HamGuy on 10/13/16.
//  Copyright © 2016 HamGuy. All rights reserved.
//

#import "HGPerson.h"

@implementation HGPerson

- (void)test{
    HGPerson *person = [[[HGPerson alloc] init] autorelease];
    person.name = @"Lucy";
    person.retainName = @"Lily";
    
    self.name = person.name;
    self.retainName = person.retainName;
    
    self.name = @"Jack";
    self.retainName = @"Mike";
    
    NSLog(@"%@",person.name);
    NSLog(@"%@",person.retainName);

    NSLog(@"#############");
    
    [person.name release];
    [person.retainName release];

    NSLog(@"%@",person.name);
    NSLog(@"%@",person.retainName);
    
}

@end
