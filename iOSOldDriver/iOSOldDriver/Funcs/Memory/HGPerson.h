//
//  HGPerson.h
//  iOSOldDriver
//
//  Created by HamGuy on 10/13/16.
//  Copyright © 2016 HamGuy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HGPerson : NSObject

@property (nonatomic, copy) NSString *name;
@property(nonatomic, strong) NSString *retainName;


-(void) test;

@end
