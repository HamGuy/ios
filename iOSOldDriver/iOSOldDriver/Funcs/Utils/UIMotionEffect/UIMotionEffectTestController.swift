//
//  UIMotionEffectTestController.swift
//  iOSOldDriver
//
//  Created by HamGuy on 10/18/16.
//  Copyright © 2016 HamGuy. All rights reserved.
//

import UIKit

class UIMotionEffectTestController: UIViewController {
    
    fileprivate lazy var backgroundImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "beautyBg"))
        imageView.contentMode = .scaleAspectFill
        imageView.frame = CGRect(x:-20,y: -20, width: UIScreen.main.bounds.width + 40, height:UIScreen.main.bounds.height + 40)
        return imageView
    }()
    
    private lazy var iconView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "evil"))
        imageView.contentMode = .scaleAspectFill
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = 8
        let sizeValue: CGFloat = 200
        let x = 0.5 * (UIScreen.main.bounds.width - sizeValue)
        let y = 0.5 * (UIScreen.main.bounds.height - sizeValue)
        imageView.frame = CGRect(x:x, y:y, width: sizeValue, height: sizeValue)
        return imageView
    }()
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(backgroundImageView)
        view.addSubview(iconView)
        addMotionEffect()
    }
    
    fileprivate func addMotionEffect(){
        let backgroundImageViewMotionEffecctX: UIInterpolatingMotionEffect = {
            let effect = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
            effect.minimumRelativeValue = -20
            effect.maximumRelativeValue = 20
            return effect
        }()
        
        let backgroundImageViewMotionEffecctY: UIInterpolatingMotionEffect = {
            let effect = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
            effect.minimumRelativeValue = -20
            effect.maximumRelativeValue = 20
            return effect
        }()
        
        let iconViewMotionEffecctX: UIInterpolatingMotionEffect = {
            let effect = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
            effect.minimumRelativeValue = -10
            effect.maximumRelativeValue = 10
            return effect
        }()
        
        let iconViewMotionEffecctY: UIInterpolatingMotionEffect = {
            let effect = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
            effect.minimumRelativeValue = -10
            effect.maximumRelativeValue = 10
            return effect
        }()
        
        let bgMotionEffectGroup = UIMotionEffectGroup()
        bgMotionEffectGroup.motionEffects = [backgroundImageViewMotionEffecctX,backgroundImageViewMotionEffecctY]
        backgroundImageView.addMotionEffect(bgMotionEffectGroup)
        
        let iconMotionEffectGroup = UIMotionEffectGroup()
        iconMotionEffectGroup.motionEffects = [iconViewMotionEffecctX, iconViewMotionEffecctY]
        iconView.addMotionEffect(iconMotionEffectGroup)
    }
}
