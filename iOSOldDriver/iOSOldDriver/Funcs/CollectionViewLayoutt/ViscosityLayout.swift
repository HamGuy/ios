//
//  ViscosityLayout.swift
//  iOSOldDriver
//
//  Created by HamGuy on 10/14/16.
//  Copyright © 2016 HamGuy. All rights reserved.
//

import UIKit

protocol ViscosityLayoutDelegate: UICollectionViewDelegateFlowLayout {
    
}

class ViscosityLayout: UICollectionViewFlowLayout {
    
    var delegate: ViscosityLayoutDelegate?
    
    var itemMargin:CGFloat = 20
    
    override func prepare() {
        super.prepare()
        
        //        guard let collectionView = collectionView, let theDeleagte = delegate else{
        //            return
        //        }
        scrollDirection = .horizontal
        minimumLineSpacing = itemMargin
        sectionInset = UIEdgeInsets(top: 0, left: itemMargin, bottom: 0, right: itemMargin)
    }
    
    
    
    /// 控制最后UICollectionView的最后去哪里
    ///用来设置UICollectionView停止滚动那一刻的位置
    ///
    /// - parameter proposedContentOffset: 原本UICollectionView停止滚动那一刻的位置
    /// - parameter velocity:              滚动速度
    ///
    /// - returns: 停止滚动那一刻的位置
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        guard let collectionView = collectionView else { return proposedContentOffset }
        
        //1.计算scrollview最后停留的范围
        let lastRect = CGRect(origin: proposedContentOffset, size: collectionView.bounds.size)
        
        //2.取出这个范围内的第一个属性,
        if let oldAttributes = layoutAttributesForElements(in: lastRect), let firstAttributes = oldAttributes.first{
            
            let startX = proposedContentOffset.x
            var adjustedOffsetX = CGFloat.greatestFiniteMagnitude
            
            let attrX = firstAttributes.frame.minX
            let attrW = firstAttributes.frame.width
            
            if startX - attrX < attrW/2{
                adjustedOffsetX = -(startX-attrX + itemMargin)
            }else{
                adjustedOffsetX = attrW - (startX - attrX)
            }
            
            return CGPoint(x: proposedContentOffset.x+adjustedOffsetX, y: proposedContentOffset.y)
        }
        return proposedContentOffset
    }
    
    //只要显示的边界发生改变，就需要重新布局：(会自动调用layoutAttributesForElementsInRect方法，获得所有cell的布局属性)
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        guard let collectionView = collectionView else{
            return super.layoutAttributesForElements(in: rect)
        }

        if let oldAttributes = super.layoutAttributesForElements(in: rect){
            let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.bounds.size)
            let centerX = collectionView.contentOffset.x + collectionView.width/2
            
            for attributes in oldAttributes{
                if !visibleRect.intersects(attributes.frame){
                    continue
                }
                let itemCenterx = attributes.center.x
                let distanceToCenterx = itemCenterx - centerX
                
                let scale = 1 + (1 - fabs(distanceToCenterx / collectionView.width * 0.5 ) * 0.7)
                attributes.transform3D = CATransform3DMakeScale(scale, scale, 1.0)
               
            }
            return oldAttributes
        }
        return nil
    }
}
