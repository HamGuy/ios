//
//  EqualSpaceHeaderView.swift
//  iOSOldDriver
//
//  Created by HamGuy on 10/18/16.
//  Copyright © 2016 HamGuy. All rights reserved.
//

import UIKit

class EqualSpaceHeaderView: UICollectionReusableView {
    
    let label:UILabel
    
    override init(frame: CGRect) {
        label = UILabel(frame: CGRect(origin: CGPoint(x:15, y:0), size: CGSize(width:frame.width - 30, height: frame.height)))
        label.backgroundColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 17.0)
        super.init(frame: frame)
        addSubview(label)
        backgroundColor = .white
        
        label.snp.makeConstraints { make in
            make.left.equalTo(self.snp.left).offset(15)
            make.width.equalTo(self.snp.width ).offset(30)
            make.height.equalTo(self.snp.height)
            make.top.equalTo(self.snp.top).offset(0)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
