//
//  EqualSpaceLayout.Swift
//  iOSOldDriver
//
//  Created by HamGuy on 10/12/16.
//  Copyright © 2016 HamGuy. All rights reserved.
//

import UIKit

extension UICollectionViewLayoutAttributes{
   fileprivate func leftAlignFrame(withInsert inset:UIEdgeInsets){
        var tmp = frame
        tmp.origin.x = inset.left
        frame = tmp
    }
}

protocol EqualSpaceLayoutDelegate: UICollectionViewDelegateFlowLayout{
    
}

class EqualSpaceLayout: UICollectionViewFlowLayout{
    
    var delegate: EqualSpaceLayoutDelegate?
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        guard let collectionView = collectionView else { return nil}
        
        let currentAttributes = super.layoutAttributesForItem(at: indexPath).map  { $0 }
        let inset = evaluatedSectionInsetForItem(at: indexPath.section)
        let isFirstItem = indexPath.item == 0
        let layoutWidth = collectionView.bounds.size.width - inset.left - inset.right
        
        if isFirstItem{
            currentAttributes?.leftAlignFrame(withInsert: inset)
            return currentAttributes
        }
        
        let previousInedexPath = IndexPath(item: indexPath.item - 1, section: indexPath.section)
        if let previousFrame = layoutAttributesForItem(at: previousInedexPath)?.frame{
            let previousFrameRightPoint = previousFrame.origin.x + previousFrame.width
            if let currentFrame = currentAttributes?.frame{
                let strecthedCurrentFrame = CGRect(x: inset.left, y: currentFrame.origin.y, width: layoutWidth, height: currentFrame.height)
                
                let isFirstItemInRow = !previousFrame.intersects(strecthedCurrentFrame)
                
                if isFirstItemInRow{
                    currentAttributes?.leftAlignFrame(withInsert: inset)
                    return currentAttributes
                }
                
                if var frame = currentAttributes?.frame{
                    frame.origin.x = previousFrameRightPoint + evaluatedMinimumInteritemSpacing(forSection: indexPath.section)
                    currentAttributes?.frame = frame
                    return currentAttributes
                }
            }
        }
        return currentAttributes
    }
    
    
    fileprivate func  evaluatedMinimumInteritemSpacing(forSection section:Int) -> CGFloat {
        if let collectionView = collectionView ,let layoutDelegate = collectionView.delegate as? UICollectionViewDelegateFlowLayout,
            let _ = layoutDelegate.collectionView?(collectionView, layout: self, minimumInteritemSpacingForSectionAt: section),
            let theDelegate = delegate{
            return theDelegate.collectionView!(collectionView, layout: self, minimumInteritemSpacingForSectionAt: section)
        }else{
            return minimumInteritemSpacing
        }
    }
    
    
    fileprivate func evaluatedSectionInsetForItem(at index:Int) -> UIEdgeInsets{
        if let collectionView = collectionView ,let layoutDelegate = collectionView.delegate as? UICollectionViewDelegateFlowLayout,
            let _ = layoutDelegate.collectionView?(collectionView, layout: self, insetForSectionAt: index),
            let theDelegate = delegate{
            return theDelegate.collectionView!(collectionView, layout: self, insetForSectionAt: index)
        }else{
            return sectionInset
        }
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        
        if let originalAttributes = super.layoutAttributesForElements(in: rect){
            var updatedAttributes = originalAttributes.map{ $0 }
            
            for arrtributes in originalAttributes{
                if arrtributes.representedElementKind == nil,  let index = updatedAttributes.index(of: arrtributes){
                    updatedAttributes[index] = layoutAttributesForItem(at: arrtributes.indexPath)!
                }
            }
            
            return updatedAttributes
        }
        return nil
    }
}
