//
//  CollectionViewLayoutsController.swift
//  iOSOldDriver
//
//  Created by HamGuy on 10/17/16.
//  Copyright © 2016 HamGuy. All rights reserved.
//

import UIKit

class CollectionViewLayoutsController: UITableViewController {

    var layouts: [String] = ["EqualSpacing", "Viscosity"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "layoutCell")
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return layouts.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "layoutCell", for: indexPath)
        cell.textLabel?.text = layouts[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        var controller:UIViewController?
        switch indexPath.row {
        case 0:
            controller = EqualSpaceCollectionViewController()
        case 1:
            controller = ViscosityViewController()
        default:
            break
        }
        if let theController = controller{
            navigationController?.pushViewController(theController, animated: true)
        }
    }
    
}
