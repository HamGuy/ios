//
//  EqualSpaceCollectionViewController.swift
//  iOSOldDriver
//
//  Created by HamGuy on 10/12/16.
//  Copyright © 2016 HamGuy. All rights reserved.
//

import UIKit
import  HGToolKit

class EqualSpaceCollectionViewController: UIViewController, EqualSpaceLayoutDelegate{
    
     var equalSpaceLayput:EqualSpaceLayout!
     var collectionView:UICollectionView!
    
    fileprivate var englishNames :[String] = []
    fileprivate var chineseNames: [String] = []
    fileprivate var showHeader = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for _ in 0...30{
            englishNames.append(UserNameFactory.make(country: .unitedStates))
            chineseNames.append(UserNameFactory.make(country: .china))
        }
        
        equalSpaceLayput = EqualSpaceLayout()
        
        collectionView = UICollectionView(frame: view.bounds, collectionViewLayout: equalSpaceLayput)
        view.addSubview(collectionView)
        collectionView.backgroundColor = UIColor.white
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.snp.makeConstraints { make in
            make.edges.equalTo(self.view.snp.edges)
        }
        
        collectionView.register(EqualSpaceCollectionViewCell.nib(), forCellWithReuseIdentifier: EqualSpaceCollectionViewCell.reuseIdentifier())
        collectionView.register(EqualSpaceHeaderView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: EqualSpaceHeaderView.reuseIdentifier())
        
        equalSpaceLayput.delegate = self
        equalSpaceLayput.minimumInteritemSpacing = 15
        equalSpaceLayput.sectionInset = UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
        
    
        hg_AddRightBarButtonItemText("显示 Header", action: #selector(showOrHideHeader(sender:)))
        
    }
    
    @IBAction  func  showOrHideHeader(sender: UIBarButtonItem){
        var buttonTitle = ""
        if sender.title == "显示 Header"{
            
            buttonTitle = "隐藏 Header"
            showHeader = true
        }else{
            buttonTitle = "显示 Header"
            showHeader = false
        }
        sender.title = buttonTitle
        collectionView.reloadData()
    }
}

extension EqualSpaceCollectionViewController: UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return showHeader ? 2 : 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return englishNames.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: EqualSpaceCollectionViewCell.reuseIdentifier(), for: indexPath) as! EqualSpaceCollectionViewCell
        cell.label.text = indexPath.section == 0 ? englishNames[indexPath.item] : chineseNames[indexPath.item]
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.borderWidth = 0.5
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionElementKindSectionHeader{
            if let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: EqualSpaceHeaderView.reuseIdentifier(), for: indexPath) as? EqualSpaceHeaderView{
                    header.label.text = indexPath.section == 0 ? "英文名字" : "中文名字"
                    return header
            }
        }
        return UICollectionReusableView()
    }
}

extension EqualSpaceCollectionViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let text = englishNames[indexPath.item]
        let width = ceil(text.hg_AttributedStringSize((lineSpace: 0, font: UIFont.systemFont(ofSize: 17)), limitedSize: CGSize(width: .greatestFiniteMagnitude, height: 40.0)).width)
        return CGSize(width:width , height: 40.0)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return showHeader ? CGSize(width: collectionView.width, height: 44) : .zero
    }
}

