//
//  ViscosityViewController.swift
//  iOSOldDriver
//
//  Created by HamGuy on 10/17/16.
//  Copyright © 2016 HamGuy. All rights reserved.
//

import UIKit
import SnapKit
import HGToolKit

class ViscosityViewController: UIViewController , ViscosityLayoutDelegate{
    
    var collectionView:UICollectionView!
    
    var colors:[UIColor] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let layout = ViscosityLayout()
        layout.itemMargin = 50
        layout.delegate = self
        layout.itemSize = CGSize(width: 90, height: 90)
        
        collectionView = UICollectionView(frame: view.bounds, collectionViewLayout: layout)
        collectionView.height = 240
        view.backgroundColor = UIColor.white
        view.addSubview(collectionView)
        collectionView.snp.makeConstraints { make in
            make.left.equalTo(0)
            make.top.equalTo(100)
            make.height.equalTo(240)
            make.width.equalTo(self.view.width)
        }
        
        collectionView.snp.updateConstraints { make in
            make.height.equalTo(240)
        }
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: UICollectionReusableView.reuseIdentifier("custom"))
        
        for _ in 0 ..< 10{
            colors.append(UIColor.randomColor())
        }
        
    }

}

extension ViscosityViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return colors.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UICollectionReusableView.reuseIdentifier("custom"), for: indexPath)
        cell.backgroundColor = colors[indexPath.item]
        return cell
    }
}
