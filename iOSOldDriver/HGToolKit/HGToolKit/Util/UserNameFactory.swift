//
//  UserNameFactory.swift
//  HGToolKit
//
//  Created by HamGuy on 9/29/16.
//  Copyright © 2016 HamGuy. All rights reserved.
//

import Foundation

/*
 Albania
 Argentina
 Armenia
 Australia
 Austria
 Azerbaijan
 Bangladesh
 Belgium
 Bosnia and Herzegovina
 Brazil
 Canada
 China
 Colombia
 Denmark
 Egypt
 England
 Estonia
 Finland
 France
 Georgia
 Germany
 Greece
 Hungary
 India
 Iran
 Israel
 Italy
 Japan
 Korea
 Mexico
 Morocco
 Netherlands
 New Zealand
 Nigeria
 Norway
 Pakistan
 Poland
 Portugal
 Romania
 Russia
 Slovakia
 Slovenia
 Spain
 Sweden
 Switzerland
 Turkey
 Ukraine
 United States
 Vietnam
 */

public enum Country: String {
    case albania = "Albania"
    case argentina = "Argentina"
    case armenia = "Armenia"
    case australia = "Australia"
    case austria = "Austria"
    case azerbaijan = "Azerbaijan"
    case bangladesh = "Bangladesh"
    case belgium = "Belgium"
    case bosniaAndHerzegovina = "Bosnia and Herzegovina"
    case brazil = "Brazil"
    case canada = "Canada"
    case china = "China"
    case colombia = "Colombia"
    case denmark = "Denmark"
    case egypt = "Egypt"
    case england = "England"
    case estonia = "Estonia"
    case finland = "Finland"
    case france = "France"
    case georgia = "Georgia"
    case germany = "Germany"
    case greece = "Greece"
    case hungary = "Hungary"
    case india = "India"
    case iran = "Iran"
    case israel = "Israel"
    case italy = "Italy"
    case japan = "Japan"
    case korea = "Korea"
    case mexico = "Mexico"
    case morocco = "Morocco"
    case netherlands = "Netherlands"
    case newZealand = "New Zealand"
    case nigeria = "Nigeria"
    case norway = "Norway"
    case pakistan = "Pakistan"
    case poland = "Poland"
    case portugal = "Portugal"
    case romania = "Romania"
    case russia = "Russia"
    case slovakia = "Slovakia"
    case slovenia = "Slovenia"
    case spain = "Spain"
    case sweden = "Sweden"
    case switzerland = "Switzerland"
    case turkey = "Turkey"
    case ukraine = "Ukraine"
    case unitedStates = "United States"
    case vietnam = "Vietnam"
}

public enum Gender: String{
    case male = "male"
    case female = "female"
    case any = "any"
}

open class UserNameFactory: NSObject{
    
    static var namesInfo: [[String:Any]] = []
    
    static let namesSouldNotSeprators = [Country.china, Country.japan, Country.korea]
    
    open class func make(country: Country, gender: Gender = .any) -> String{
        var theGender = gender
        let info = namesInfo(forCountry: country)
        
        if theGender == .any{
            theGender =  Int(arc4random()) % 2 == 0 ? .male : .female
        }
        
        let firstNames = info[theGender.rawValue] as! [String]
        let surnames = info["surnames"] as! [String]
        
        let  nameRandomNumber = Int(arc4random()) % firstNames.count
        let  surnameRandomNumber = Int(arc4random()) % surnames.count
        
        let firstname = firstNames[nameRandomNumber]
        let surname = surnames[surnameRandomNumber]
        
        if country == .china{
            return "\(surname)\(firstname)"
        }
        
        return namesSouldNotSeprators.contains(country) ? "\(firstname)\(surname)" : "\(firstname) \(surname)"
    }
    
    
    fileprivate  class func namesInfo(forCountry country: Country) -> [String: Any]{
        if namesInfo.count == 0{
            if let bundlePath  =  Bundle(for: UserNameFactory.self).path(forResource: "HGToolKit", ofType: "bundle") ,
                let bundle = Bundle(path: bundlePath),
                let filePath = bundle.path(forResource: "names", ofType: "plist") {
                namesInfo =  NSArray(contentsOfFile: filePath) as! [[String:Any]]
            }
        }
        let filter =  namesInfo.filter{ ($0["region"] as! String) ==  country.rawValue}
        return filter.first ?? [:]
    }
    
    
}


