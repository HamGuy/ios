//
//  PasswordFactory.swift
//  HGToolKit
//
//  Created by HamGuy on 9/29/16.
//  Copyright © 2016 HamGuy. All rights reserved.
//

import Foundation


open class PasswordFactory: NSObject {
    
    
    //https://www.microsoft.com/china/smb/issues/sgc/articles/select_sec_passwords.mspx
    fileprivate static let special = "!()-.,+=_`~@#$%^&*|/{}[];?<>"  //33-47  58-96  123-126
    fileprivate static let digitals = "0123456789"  // ascii 48 - 57
    fileprivate static let lowerLetters = "abcdefghijklmnopqrst"  // 大写 ascii 65 - 90 小写 97-122
    fileprivate static let upperLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    
    /// 生成密码
    ///
    /// - parameter length:                密码长度
    /// - parameter digital:          是否允许数字
    /// - parameter specialCharatars: 是否允许特殊字符 （*#@,.-_~`）
    /// - parameter capital: 是否允许大写
    ///
    /// - returns: 生成的密码
   open  class func make(length: Int, digital: Bool = true , specialCharatars: Bool = true, capital: Bool = true) -> String{
        var letters =  lowerLetters
        if digital{
             letters += digitals
        }
        if specialCharatars{
            letters += special
        }
        
        if capital{
            letters += upperLetters
        }
        
        let array = Array(letters.characters)
        var result = ""
        for _ in 0 ..< length {
            result.append(array[randNumber(bound: UInt32(array.count))])
        }
        
        return result
    }
    
    fileprivate class func randNumber(bound: UInt32) -> Int{
        return Int(arc4random_uniform(bound))
    }
    
}
