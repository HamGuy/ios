//
//  PlistFileManager.swift
//  HGToolKit
//
//  Created by HamGuy on 9/29/16.
//  Copyright © 2016 HamGuy. All rights reserved.
//

import Foundation

public struct PlistFile {
    public enum TargetType: Int{
        case array
        case dictonary
    }
    
    let fileName: String
    let targetType: TargetType
    
    fileprivate var array: [Any]?
    fileprivate var dictonary:[String:Any]?
    
  public  var dictnoryValue:[String:Any]{
        return targetType == .dictonary ? dictonary ?? [:] :  [:]
    }
    
   public var arrayValue:[Any]{
        return targetType == .array ? array ?? [] :  []
    }
    
    public init(fileName:String, targetType: TargetType = .array) {
        self.fileName = fileName
        self.targetType = targetType
        load()
    }
    
    fileprivate mutating func load(){
        let name = fileName.hasSuffix(".plist") ? fileName.subStringToIndex(endIndex: fileName.length-6) : fileName
        if let filePath  = Bundle.main.path(forResource: name, ofType: "plist"), FileManager.default.fileExists(atPath: filePath){
            if targetType == .array{
                array = NSArray(contentsOfFile: filePath) as? [Any]
                }else{
                    dictonary = NSDictionary(contentsOfFile: filePath) as? [String:Any]
                }
            }
        }
}
