//
//  UIApplication.swift
//  HGToolKit
//
//  Created by HamGuy on 10/1/16.
//  Copyright © 2016 HamGuy. All rights reserved.
//

import UIKit

extension UIApplication{
    class func hg_topViewController(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return hg_topViewController(nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return hg_topViewController(selected)
            }
        }
        if let presented = base?.presentedViewController {
            return hg_topViewController(presented)
        }
        return base
    }

}
