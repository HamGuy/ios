//
//  UIColor.swift
//  dakuofu
//
//  Created by HamGuy on 8/31/16.
//  Copyright © 2016 HamGuy. All rights reserved.
//

import UIKit


extension UIColor{
   public convenience init(decRed: CGFloat, decGreen: CGFloat, decBlue: CGFloat) {
        self.init(red: decRed/255.0, green: decGreen/255.0, blue: decBlue/255.0, alpha: 1.0)
    }
    
  public  convenience init(decRed: CGFloat, decGreen: CGFloat, decBlue: CGFloat, alpha: CGFloat) {
        self.init(red: decRed/255.0, green: decGreen/255.0, blue: decBlue/255.0, alpha: alpha)
    }
    
  public  class func randomColor()->UIColor{
        let r =  CGFloat(drand48())
        let g =  CGFloat(drand48())
        let b =  CGFloat(drand48())
        return UIColor(red: r, green: g, blue: b, alpha: 1.0)
    }
    
}

