//
//  UIView.swift
//  HGToolKit
//
//  Created by HamGuy on 10/1/16.
//  Copyright © 2016 HamGuy. All rights reserved.
//

import UIKit

extension UIView{
   public var width: CGFloat {
        get {
            return self.frame.size.width
        }
        set {
            self.frame.size.width = newValue
        }
    }
    
   public var height: CGFloat {
        get {
            return self.frame.size.height
        }
        set {
            self.frame.size.height = newValue
        }
    }
    
  public  var size: CGSize {
        get {
            return self.frame.size
        }
        set {
            self.frame.size = newValue
        }
    }
    
   public var origin: CGPoint {
        get {
            return self.frame.origin
        }
        set {
            self.frame.origin = newValue
        }
    }
    
  public  var x: CGFloat {
        get {
            return self.frame.origin.x
        }
        set {
            self.frame.origin = CGPoint(x: newValue, y: self.frame.origin.y)
        }
    }
    
  public  var y: CGFloat {
        get {
            return self.frame.origin.y
        }
        set {
            self.frame.origin = CGPoint(x: self.frame.origin.x, y: newValue)
        }
    }
    
  public  var centerX: CGFloat {
        get {
            return self.center.x
        }
        set {
            self.center = CGPoint(x: newValue, y: self.center.y)
        }
    }
    
 public   var centerY: CGFloat {
        get {
            return self.center.y
        }
        set {
            self.center = CGPoint(x: self.center.x, y: newValue)
        }
    }
    
  public  var left: CGFloat {
        get {
            return self.frame.origin.x
        }
        set {
            self.frame.origin.x = newValue
        }
    }
    
  public  var right: CGFloat {
        get {
            return self.frame.origin.x + self.frame.size.width
        }
        set {
            self.frame.origin.x = newValue - self.frame.size.width
        }
    }
    
 public   var top: CGFloat {
        get {
            return self.frame.origin.y
        }
        set { self.frame.origin.y = newValue }
    }
    
  public  var bottom: CGFloat {
        get {
            return self.frame.origin.y + self.frame.size.height
        }
        set {
            self.frame.origin.y = newValue - self.frame.size.height
        }
    }
    
  public  func subviewWithTag(_ tag: Int) -> UIView?{
        for v in subviews{
            if v.tag == tag{
                return v
            }
        }
        return nil
    }
    
    public var angle: CGFloat{
        get{
            let radians:CGFloat = atan2( transform.b, transform.a)
            let degrees = radians * (CGFloat(180.0) / CGFloat.pi )
            return degrees
        }
    }
}

extension UIView{
    @discardableResult
 public   func hg_addLeftBorder(_ color:UIColor, thickness:CGFloat, sideInset:(top:CGFloat, bottom:CGFloat) = (0,0))->CALayer{
        let border = CALayer()
        border.backgroundColor = color.cgColor
        
        border.frame = CGRect(x: 0,y: sideInset.top,width: thickness,height: self.height-(sideInset.top+sideInset.bottom))
        self.layer.addSublayer(border)
        return border
    }
    
    @discardableResult
  public  func hg_addTopBorder(_ color:UIColor, thickness:CGFloat,sideInset:(left:CGFloat, right:CGFloat) = (0,0))->CALayer{
        let border = CALayer()
        border.backgroundColor = color.cgColor
        
        border.frame = CGRect(x: sideInset.left,y: 0,width: self.width-(sideInset.left+sideInset.right),height: thickness)
        self.layer.addSublayer(border)
        return border
    }
    
    @discardableResult
 public   func hg_addRightBorder(_ color:UIColor, thickness:CGFloat, sideInset:(top:CGFloat, bottom:CGFloat) = (0,0))->CALayer{
        let border = CALayer()
        border.backgroundColor = color.cgColor
        
        border.frame = CGRect(x: self.width-thickness,y: sideInset.top,width: thickness,height: self.height-(sideInset.top+sideInset.bottom))
        self.layer.addSublayer(border)
        return border
    }
    
    @discardableResult
  public  func hg_addBottomBorder(_ color:UIColor, thickness:CGFloat,sideInset:(left:CGFloat, right:CGFloat) = (0,0))->CALayer{
        let border = CALayer()
        border.backgroundColor = color.cgColor
        
        border.frame = CGRect(x: sideInset.left,y: self.height-thickness, width: self.width-(sideInset.left+sideInset.right),height: thickness)
        self.layer.addSublayer(border)
        return border
    }
    
    @discardableResult
    public func hg_addBorder(_ color:UIColor, thickness:CGFloat,sideInset:(left:CGFloat, right:CGFloat) = (0,0)) -> (letft:CALayer,top:CALayer,right:CALayer,bottom:CALayer){
        let left = hg_addLeftBorder(color, thickness: thickness)
        let top = hg_addTopBorder(color, thickness: thickness)
        let right =  hg_addRightBorder(color, thickness: thickness)
        let bottom =   hg_addBottomBorder(color, thickness: thickness)
        return (left,top,right,bottom)
    }
}


// MARK: - animation
extension UIView{
  public  func hg_FadeTransition(_ duration:CFTimeInterval) {
        layer.removeAnimation(forKey: "hgFade")
        let animation:CATransition = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
            kCAMediaTimingFunctionEaseInEaseOut)
        animation.type = kCATransitionFade
        animation.duration = duration
        layer.add(animation, forKey: "hgFade")
    }
    
  public  func hg_ShakeAnimation(_ duration:TimeInterval = 0.03){
        layer.removeAnimation(forKey: "hgShake")
        let animation = CABasicAnimation(keyPath: "position")
        animation.repeatCount = 6
        animation.duration = duration
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - self.width*0.01, y: self.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + self.width*0.01, y: self.center.y))
        layer.add(animation, forKey: "hgShake")
    }
    
    public func hg_RotateAnimation(targetAngle angel:CGFloat, duration:TimeInterval = 0.03){
        layer.removeAnimation(forKey: "hgRotate")
        let animtion = CABasicAnimation(keyPath: "transform.rotation.z")
        animtion.toValue = angel
        layer.add(animtion, forKey: "hgRotate")
    }
}
