//
//  Array.swift
//  dakuofu
//
//  Created by HamGuy on 9/8/16.
//  Copyright © 2016 HamGuy. All rights reserved.
//

import Foundation

extension Array{
    public subscript (safe index: Int) -> Element? {
        return indices ~= index ? self[index] : nil
    }
    
    public mutating func removeObject<U: Equatable>(_ object: U) {
        var index: Int?
        for (idx, objectToCompare) in self.enumerated() {
            if let to = objectToCompare as? U {
                if object == to {
                    index = idx
                }
            }
        }
        
        if(index != nil) {
            self.remove(at: index!)
        }
    }
}



// MARK: - Plist handing
extension NSArray{
    public class func hg_arrayWith(plistData data: Data) -> NSArray?{
            do{
                let array = try PropertyListSerialization.propertyList(from: data, options: [], format: nil)
                return array as? NSArray
            }catch {
                return nil
            }
        
    }
    
    public class func hg_arrayWith(plistString pString:String) -> NSArray?{
        if let data = pString.data(using: .utf8){
            return hg_arrayWith(plistData: data)
        }
        return nil
    }
    
    public var hg_PlistData: Data?{
        do {
         return try PropertyListSerialization.data(fromPropertyList: self, format: .binary, options: 0)
        }catch{
            return nil
        }
    }
    
    public var hg_PlistString: String?{
        do{
            let data = try PropertyListSerialization.data(fromPropertyList: self, format: .xml, options: 0)
            return String(data: data, encoding: .utf8)
        }catch{
            return nil
        }
    }
}

